package com.sun.myformwork.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 模仿springMVC的controller注解
 * 其本质是一个标志标志注解,方便框架筛选出该类
 * value值得规则为 /项目路径/value值.do?method=被@Controller注释类的方法
 * 例如 /app/login.do?method=login
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Controller {
    String value();
}
