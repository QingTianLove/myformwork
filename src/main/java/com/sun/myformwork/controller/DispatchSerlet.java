package com.sun.myformwork.controller;

import com.sun.myformwork.annotation.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 框架核心服务
 *1、接受全部服务请求完成请求的分发
 *2、扫描全部类筛选出全部含有@Controller注解的类根据注解及雷德方法分发请求并执行其方法
 */
@WebServlet(name="dispatchSerlet",urlPatterns="*.do")
public class DispatchSerlet  extends HttpServlet {
    private static Map<String,Object> urlMap = new HashMap<String,Object>();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       req.setCharacterEncoding("UTF-8");
       resp.setContentType("text/html;charset=UTF-8");
       String uri = req.getRequestURI();
       uri = uri.replaceAll("/","");
       String context = req.getContextPath().replaceAll("/","");
       if(uri == context){
           req.getRequestDispatcher("/index.jsp").forward(req, resp);
           return;
       }
       String key = uri.substring(context.length(), uri.length()-3);
       Object action = urlMap.get(key);
       if (action == null){
           req.getRequestDispatcher("/index.jsp").forward(req, resp);
           return;
       }
       String method = req.getParameter("method");
       if (method != null){
           try {
               Method m = action.getClass().getMethod(method,HttpServletRequest.class,HttpServletResponse.class);
               m.invoke(action,req,resp);
           } catch (NoSuchMethodException e) {
               e.printStackTrace();
           } catch (IllegalAccessException e) {
               e.printStackTrace();
           } catch (InvocationTargetException e) {
               e.printStackTrace();
           }
       }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    /**
     * 核心服务生命周期方法
     * 1、扫描含@Controller的类
     * 2、将注解value值与被注解类相互映射并保存在一个Map集合中
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        //WEB-INF\classes
        String classPath = this.getServletContext().getRealPath("/WEB-INF/classes");
        try {
            scanClass(new File(classPath));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }catch (InstantiationException e){
            e.printStackTrace();
        }catch (IllegalAccessException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 1、扫描指定路径下包含@Controller注解的类
     * 2、将注解的value值与该类的映射保存在urlMap集合中
     * @param path 指定扫描的路径
     */
    private void scanClass(File path) throws ClassNotFoundException, InstantiationException, IllegalAccessException,IOException{
        if (path.isFile()){
            if(path.getPath().endsWith(".class")){
                MyClassLoader classLoader = new MyClassLoader(DispatchSerlet.class.getClassLoader());
                Class<?> clazz = classLoader.loadClass(path);
                if (clazz != null){
                    Controller controller = clazz.getAnnotation(Controller.class);
                    if(controller != null){
                        String url = controller.value();
                        url = url.replaceFirst("/","");
                        urlMap.put(url,clazz.newInstance());
                    }
                }

            }
        }else{
            File[] files = path.listFiles();
            for(File file : files){
                scanClass(file);
            }

        }
    }

    private class MyClassLoader extends ClassLoader{
        protected MyClassLoader(ClassLoader parent) {
            super(parent);
        }

        public Class<?> loadClass(File path) throws ClassNotFoundException,IOException{
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            FileInputStream fis = new FileInputStream(path);
            byte[] buff = new byte[1024];
            int len = 0;
            while((len = fis.read(buff)) > 0){
                baos.write(buff,0,len);
            }
            baos.flush();
            fis.close();
            byte[] bin = baos.toByteArray();
            return super.defineClass(null,bin,0,bin.length);
        }
    }
}
